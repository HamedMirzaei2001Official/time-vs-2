from datetime import datetime

class Time:
    time = str(datetime.now())

    def __init__(self, hour=int(time[11:13]), minute=int(time[14:16]), second=int(time[17:19])):
        self.hour = hour
        self.minute = minute
        self.second = second

    def set_time(self, hour, minute, second):
        self.hour = hour
        self.minute = minute
        self.second = second


    def to_str(self):
        return f"{self.hour}:{self.minute}:{self.second}"

    @classmethod
    def add(cls, time1, time2):
        s = time1.second + time2.second
        m = time1.minute + time2.minute + (s / 60)
        h = time1.hour + time2.hour + (m / 60)
        s %= 60
        m %= 60
        return Time(int(h), int(m), int(s))

    @classmethod
    def difference(cls, time1, time2):
        if time2.second > time1.second:
            time1.minute -= 1
            time1.second += 60

        if time2.minute > time1.minute:
            time1.hour -= 1
            time1.minute += 60

        s = time1.second - time2.second
        m = time1.minute - time2.minute
        h = time1.hour - time2.hour

        return Time(int(h), int(m), int(s))

    @classmethod
    def parse(cls, time_str):
        return Time(time_str[0:2], time_str[3:5], time_str[6:8])

    def to_seconds(self):
        seconds = 0
        seconds += self.hour * 3600
        seconds += self.minute * 60
        seconds += self.second
        return seconds


if __name__ == "__main__":
    d1 = Time()
    d2 = Time()
    d1.set_time(21, 25, 57)
    d2.set_time(10, 22, 42)
    print(d1.to_str())
    print(d2.to_str())
    d3 = Time.add(d1, d2)
    print(f"{d3.to_str():}")
    d4 = Time.difference(d1, d2)
    print(d4.to_str())
    time_str = "14:27:59"
    d6 = Time.parse(time_str)
    print(d6.to_str())
    print(d1.to_seconds())
